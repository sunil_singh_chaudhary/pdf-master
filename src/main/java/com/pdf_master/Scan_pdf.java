package com.pdf_master;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Scan_pdf extends FragmentManagePermission {
    public int TAKE_CAMERA_PICTURE = 101;
    private ImageLoader imageLoader;
    public static File folder;
    private File fewfiletest;
    private Bitmap bmps_rotate;
    private Document document;
    private ByteArrayOutputStream stream;
    private ImageView imagev_pdf;
    private EditText text_body_pdf, header_edit_pdf;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_scan_pdf, container, false);
        ((MainActivity) getActivity())
                .getSupportActionBar().setTitle(R.string.scanpdf);
      // Utilities.InterstialAddMobb1(getActivity());
        askCompactPermissions(new String[]{PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE}, new PermissionResult() {
            @Override
            public void permissionGranted() {
            }

            @Override
            public void permissionDenied() {
            }
        });
        text_body_pdf = (EditText) view.findViewById(R.id.text_body_pdf);
        header_edit_pdf = (EditText) view.findViewById(R.id.header_pdf);
        imagev_pdf = (ImageView) view.findViewById(R.id.imagev_pdf);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration
                .createDefault(getActivity()));
        imagev_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(),
                        "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, TAKE_CAMERA_PICTURE);

            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.scan_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.scan:
                String text_body_text = text_body_pdf.getText().toString();
                String header = header_edit_pdf.getText().toString();
                if (text_body_text.equals("") || header.equals("") || imagev_pdf.getDrawable() == null) {
                    Toast.makeText(getActivity(), R.string.fillall, Toast.LENGTH_LONG).show();
                } else {

                    try {
                        Image image = Image.getInstance(stream.toByteArray());
                        PdfPTable table = new PdfPTable(1);
                        Font font = new Font(FontFactory.getFont(FontFactory.TIMES_BOLD, 30));
                        font.setColor(BaseColor.WHITE);
                        PdfPCell cell = new PdfPCell(new Phrase(header, font));
                        cell.setBorderColorTop(new BaseColor(255, 0, 0));
                        cell.setBorderColorBottom(BaseColor.RED);
                        cell.setBackgroundColor(BaseColor.PINK);
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        table.addCell(cell);
                        table.setWidthPercentage(60);
                        table.addCell(PDF_cells.createImageCell(image));
                        table.addCell(new Phrase(text_body_text, FontFactory.getFont(FontFactory.COURIER, 22)));
                        document.add(table);
                        document.close();
                    } catch (DocumentException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        //Opening PDF FILE USING below intentn
                        Intent intent = new Intent(getActivity(), Custom_Pdf_viewer.class);
                        intent.putExtra("EXTRA_PDFFILENAME", fewfiletest.getAbsolutePath());
                        Log.e("sendeddddddddd", fewfiletest.getAbsolutePath() + "");
                        Toast.makeText(getActivity(),"Created", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                return false;


            default:
                break;
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_CAMERA_PICTURE) {
            if (resultCode == getActivity().RESULT_OK) {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bm = imageLoader.loadImageSync("file://"
                            + f.getAbsolutePath());
                    String filename = f.getAbsolutePath();
                    stream = new ByteArrayOutputStream();
                    Bitmap bmps = Bitmap.createScaledBitmap(bm, bm.getWidth() / 2, bm.getHeight() / 2, false);
                    CheckOrientation(filename, bmps);
                    bmps_rotate.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    imagev_pdf.setImageBitmap(bmps_rotate);
                    document = new Document();
                    fewfiletest = new File(createDirIfNotExists("pdf_img"), System.currentTimeMillis() + ".pdf");
                    PdfWriter.getInstance(document, new FileOutputStream(fewfiletest));
                    document.open();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (resultCode == getActivity().RESULT_CANCELED) {
            // user cancelled Image capture
            Toast.makeText(getActivity(),
                    R.string.can, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void CheckOrientation(String filename, Bitmap bmps) {
        ExifInterface ei;
        int orientation = 0;
        try {
            ei = new ExifInterface(filename);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bmps_rotate = Utilities.rotateImage(bmps, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bmps_rotate = Utilities.rotateImage(bmps, 180);
                break;
            default:
                Log.e("Yahan aya", "hai");
                bmps_rotate = bmps;
                // etc.
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public File createDirIfNotExists(String foldername) {
        folder = new File(Environment.getExternalStorageDirectory(),
                foldername);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();

        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
        return folder;

    }


}