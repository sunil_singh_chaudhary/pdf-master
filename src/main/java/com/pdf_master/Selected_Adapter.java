package com.pdf_master;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class Selected_Adapter extends BaseAdapter {
    private Context ctx = null;
    private ArrayList<String> items;
    private ArrayList<String> names;
    public RecordHolder holder = null;
    public int fromwhere;
    public SharedPreferences preferences;
    public static String HEADER_PATH = "headerpath";
    public static String HEADER_PATH_GET = "gethere";
    public SharedPreferences.Editor edit;
    private ArrayList<Integer> myImageList = new ArrayList<>();

    public Selected_Adapter(Context ctx, ArrayList<String> items, ArrayList<String> names, int fromwhere) {
        this.ctx = ctx;
        this.items = items;
        this.names = names;
        this.fromwhere = fromwhere;
        new ArrayList<String>();
        preferences = ctx.getSharedPreferences(HEADER_PATH, ctx.MODE_PRIVATE);
        edit = preferences.edit();
        for (int i = 0; i < items.size(); i++) {
            myImageList.add(R.drawable.pdf);
        }

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        View v = view;
        LayoutInflater mInflater = (LayoutInflater) ctx
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (v == null) {
            v = mInflater.inflate(R.layout.girdselctitems, parent, false);
            holder = new RecordHolder();
            holder.imagev_pdf =  v.findViewById(R.id.imag_v_pdf);
            holder.textname_pdf =  v.findViewById(R.id.nameofpdf_pdf);

            v.setTag(holder);
        } else {
            holder = (RecordHolder) v.getTag();
        }

        holder.imagev_pdf.setBackgroundResource(R.drawable.pdf);
        holder.textname_pdf.setText(names.get(position));
        holder.imagev_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", position + "");

                String showpdfpathis = items.get(position);
                Log.e("PDF path", items.get(position));
                if (fromwhere == 1) {
                    try {
                        //Open PDF file Using Custom PDF viewer Class
                        Intent intent = new Intent(ctx, Custom_Pdf_viewer.class);
                        intent.putExtra("EXTRA_PDFFILENAME", showpdfpathis);
                        ctx.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    getSignImageFromGallery(position);

                }
            }
        });
        holder.imagev_pdf.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle(R.string.chooser);
                builder.setItems(new CharSequence[]
                                {"Delete", "Share"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        String pdfpathis = items.get(position);
                                        items.remove(position);
                                        myImageList.remove(position);
                                        names.remove(position);
                                        deletePdfFile(pdfpathis);
                                        notifyDataSetChanged();
                                        break;
                                    case 1:
                                        String sharepdfpath = items.get(position);
                                        share(sharepdfpath);
                                        break;
                                }
                            }
                        });
                builder.create().show();
                return true;
            }
        });
        return v;
    }

    private void share(String pdfpathis) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_SUBJECT, ctx.getString(R.string.attach));
        email.putExtra(Intent.EXTRA_TEXT, ctx.getString(R.string.helllosir));
        Uri uri = Uri.parse("file://" + pdfpathis);
        email.putExtra(Intent.EXTRA_STREAM, uri);
        email.setType("message/rfc822");
        ctx.startActivity(email);
    }

    private void deletePdfFile(String path) {
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + path);
                Toast.makeText(ctx, R.string.deleted, Toast.LENGTH_LONG).show();
            } else {
                System.out.println("file not Deleted :" + path);
            }
        }
    }

    private void getSignImageFromGallery(int position) {
        Intent i = new Intent(ctx, GallerySignature.class);
        ctx.startActivity(i);
        Log.e("FROM !", "getSignImageFromGallery");
        edit.putString(HEADER_PATH_GET, items.get(position));
        edit.apply();

    }

    public static class RecordHolder {
        public ImageView imagev_pdf;
        public TextView textname_pdf;

    }
}
