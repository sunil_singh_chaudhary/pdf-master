package com.pdf_master;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by therock on 3/9/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private Fragment alldocs;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                alldocs = new Documents();

                break;
            case 1:
                alldocs = new RecentsDocs();
                break;

        }
        return alldocs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}