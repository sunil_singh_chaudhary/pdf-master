package com.pdf_master;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class Documents extends Fragment {
    private ArrayList<String> listofALL_pdf = new ArrayList<>();
    private ArrayList<String> listofALL_pdf_name = new ArrayList<>();
    public static ArrayList<String> recent_files_array = new ArrayList<>();
    public static ArrayList<String> recent_files_array_names = new ArrayList<>();
    private ArrayList<File> filteredfiles = new ArrayList<>();
    private int k = 1;
    private GridView gridView;
    private Selected_Adapter adapter;
    public TextView nopdffound;
    ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_docs_tab, container, false);
        listofALL_pdf.clear();
        listofALL_pdf_name.clear();
        gridView = (GridView) view.findViewById(R.id.gridView1);
        nopdffound = (TextView) view.findViewById(R.id.nopdffound);
        new DataSearchInBackground().execute();
     /*   AdView mAdView = (AdView) view.findViewById(R.id.adView_jai);
        adRequest = new AdRequest.Builder()
                .addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        mAdView.loadAd(adRequest);*/
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void Logmessage(String msg, String data) {
        Log.e("" + data, msg + "");
    }

    class DataSearchInBackground extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("searching..");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            walkdir(Environment.getExternalStorageDirectory());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            recent_files_array.clear();
            recent_files_array_names.clear();
            Log.e("filteredfiles", filteredfiles.size() + "");
            if (listofALL_pdf_name.size() <= 0) {
                nopdffound.setVisibility(View.VISIBLE);
                nopdffound.setText("No PDF files Found");
                Log.e("NO pdf Found", "DRAMA");
            } else {
                nopdffound.setVisibility(View.GONE);

            }
            for (int j = 0; j < filteredfiles.size(); j++) {
                if (k <= 3) {
                    if (filteredfiles.size() - 1 >= 3) {
                        try {
                            String lastindex = filteredfiles.get(filteredfiles.size() - k).getAbsolutePath();
                            recent_files_array.add(lastindex);
                            String filename = lastindex.substring(lastindex.lastIndexOf("/") + 1);
                            recent_files_array_names.add(filename);
                            k++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            adapter = new Selected_Adapter(getActivity(), listofALL_pdf, listofALL_pdf_name, 1);
            gridView.setAdapter(adapter);
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onResume() {
        Log.e("PDFSIZE", listofALL_pdf_name.size() + "");
        if (listofALL_pdf_name.size() <= 0) {
            nopdffound.setVisibility(View.VISIBLE);
            nopdffound.setText("No PDF files Found");
            Log.e("NO pdf Found", "DRAMA");
        } else {
            nopdffound.setVisibility(View.GONE);

        }
        adapter = new Selected_Adapter(getActivity(), listofALL_pdf, listofALL_pdf_name, 1);
        gridView.setAdapter(adapter);
        super.onResume();
    }

    public void walkdir(File dir) {
        String pdfPattern = ".pdf";

        File listFile[] = dir.listFiles();
        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    walkdir(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(pdfPattern)) {
                        filteredfiles.add(listFile[i]);   ///array of files shor
                        String path = listFile[i].getAbsolutePath();
                        String filename = path.substring(path.lastIndexOf("/") + 1);
                        listofALL_pdf_name.add(filename);
                        Logmessage("all pdf names  ========   ", path + "");
                        listofALL_pdf.add(path);

                    }
                }
            }
            Collections.sort(filteredfiles, new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    return Long.valueOf(file1.lastModified()).compareTo(Long.valueOf(file2.lastModified()));
                }
            });

        }
    }


}