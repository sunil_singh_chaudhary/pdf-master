package com.pdf_master;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;

public class Custom_Pdf_viewer extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {
    private PDFView pdfView;
    private String path;
    private Intent intent;
    private Context ctx;
    private int pageNumber = 1;
    private static final String TAG = Custom_Pdf_viewer.class.getSimpleName();
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom__pdf_viewer);
        intent = getIntent();
        ctx = Custom_Pdf_viewer.this;
        pdfView = (PDFView) findViewById(R.id.pdfview);
        path = intent.getStringExtra("EXTRA_PDFFILENAME");
        if (path != null) {
            try {
                file = new File(path);
                pdfView.fromFile(file)
                        .defaultPage(pageNumber)
                        .onError(new OnErrorListener() {
                            @Override
                            public void onError(Throwable t) {
                                if (t.getMessage().contains("Password required or incorrect password")) {
                                    showDialog();
                                } else {
                                    Log.e("Passsword Match", "YO");
                                }

                            }
                        })
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(new DefaultScrollHandle(this))
                        .load();
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else {
            try {
                Uri uri = getIntent().getData();
                file = new File(uri.getPath());
                pdfView.fromFile(file)
                        .defaultPage(pageNumber)
                        .onPageChange(this).onError(new OnErrorListener() {
                    @Override
                    public void onError(Throwable t) {
                        if (t.getMessage().contains("Password required or incorrect password")) {
                            showDialog();
                        } else {
                            Log.e("Passsword Match", "YOYO");
                        }
                    }
                }).enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(new DefaultScrollHandle(this))
                        .load();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("EXCEPTION ", e.getMessage().toString());
            }
        }

    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", "Page No-", page, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        pageNumber = nbPages;
        setTitle(String.format("%s", "Page No-", pageNumber));
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
    }

    public void showDialog() {

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.searchprompt, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.user_input);


        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Go",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                /** DO THE METHOD HERE WHEN PROCEED IS CLICKED*/
                                String password = (userInput.getText()).toString();
                                Log.e("Password enter--", password);

                                showPassword(password);
                            }
                        })
                .setPositiveButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }

                        }

                );

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void showPassword(String password) {
        Log.e("Password settt--", password);
        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .password(password)
                .scrollHandle(new DefaultScrollHandle(this))
                .onError(new OnErrorListener() {
                    @Override
                    public void onError(Throwable t) {
                        if (t.getMessage().contains("Password required or incorrect password")) {
                            showDialog();
                        }

                    }
                })
                .load();
    }
}
