package com.pdf_master;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private android.support.v4.app.FragmentTransaction transaction;
    private Fragment create_pdf, view_pdf, scan_pdf;
    private boolean doubleBackToExitPressedOnce;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private  NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer =  findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        view_pdf = new View_pdf();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_container, view_pdf);
        transaction.commit();

    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.View_pdf) {
            view_pdf = new View_pdf();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, view_pdf);
            transaction.commit();
            /*try {
                if (Utilities.mInterstitialAd1.isLoaded()) {
                    Utilities.mInterstitialAd1.show();
                    Log.e("inside-","View");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        } else if (id == R.id.create_PDF) {
            create_pdf = new Create_Pdf();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, create_pdf);
                      transaction.commit();
           /* try {
                if (Utilities.mInterstitialAd1.isLoaded()) {
                    Utilities.mInterstitialAd1.show();
                    Log.e("inside-","create_PDF");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
*/
        } else if (id == R.id.Scan_pdf) {
            scan_pdf = new Scan_pdf();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_container, scan_pdf);
                    transaction.commit();
         /*   try {
                if (Utilities.mInterstitialAd1.isLoaded()) {
                    Utilities.mInterstitialAd1.show();
                    Log.e("inside-","Scan_pdf");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        } else if (id == R.id.nav_share) {
            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.filereader));
                String sAux = getString(R.string.enjoy);
                sAux = sAux + "https://play.google.com/store/apps/details?id="+getPackageName();
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, getString(R.string.choose)));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.rate_us) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        }

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.pressagain, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }
}
