package com.pdf_master;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itextpdf.text.Font;

import java.util.ArrayList;
import java.util.List;


public class Create_Pdf extends FragmentManagePermission {
    private String Heading_First = " ";
    private FileOperations fop;
    public static EditText fcontent_pdf;
    private EditText Heading_Edittext_pdf;
    public static Button create_pdf;
    private String file_content = "null", filename;
    private int currentBackgroundColor = 0x00000000;
    public int fontfamilytype = 0;
    public int fontsize = 22;
    public int fontstyle = Font.ITALIC;
    private View view;
    private AdView mAdView;
    private AdRequest adRequest;
    private NinePatchDrawable mWorkspaceBackgroundDrawable;
    private LinearLayout fullid_pdf;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity())
                .getSupportActionBar().setTitle(R.string.createpdf);
        view = inflater.inflate(R.layout.fragment_create__pdf, container, false);

       // mWorkspaceBackgroundDrawable = (NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.backkk);
        fullid_pdf=(LinearLayout)view.findViewById(R.id.fullid_pdf);
        fullid_pdf.setBackgroundDrawable(mWorkspaceBackgroundDrawable);
        init(view);

        askCompactPermissions(new String[]{PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE}, new PermissionResult() {
            @Override
            public void permissionGranted() {
            }

            @Override
            public void permissionDenied() {
                //permission denied
                //replace with your action
            }
        });
        mAdView = (AdView) view.findViewById(R.id.adView);
        adRequest = new AdRequest.Builder()
                .addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        mAdView.loadAd(adRequest);
        create_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Heading_First = Heading_Edittext_pdf.getText().toString();
                file_content = fcontent_pdf.getText().toString();
                if (isEmpty(Heading_Edittext_pdf)) {
                    Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                } else if (isEmpty(fcontent_pdf)) {
                    Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                } else {
                    dialog();
                }

            }
        });

        return view;
    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }


    private void init(View view) {

        Heading_Edittext_pdf = (EditText) view.findViewById(R.id.fname_pdf);
        fcontent_pdf = (EditText) view.findViewById(R.id.fcontent_pdf);
        create_pdf = (Button) view.findViewById(R.id.create_pdf);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<String> content = new ArrayList<>();


    }

    private void dialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.prompt, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // set prompts.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.userInput);
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // get user input and set it to result
                        filename = input.getText().toString();
                        if (filename.isEmpty()) {
                            Toast.makeText(getActivity(), R.string.filename, Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e("Name of file", filename);
                            Heading_First = Heading_Edittext_pdf.getText().toString();
                            file_content = fcontent_pdf.getText().toString();
                            Log.e("Heading_First", Heading_First);
                            Log.e("file_content", file_content);
                            fop = new FileOperations();
                            fop.write(filename, Heading_First, file_content, fontfamilytype, fontsize, fontstyle, currentBackgroundColor);

                            if (fop.write(filename, Heading_First, file_content, fontfamilytype, fontsize, fontstyle, currentBackgroundColor)) {
                                Toast.makeText(getActivity(), Heading_First + getActivity().getString(R.string.created), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(getActivity(), R.string.error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancels,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Heading_Edittext_pdf.setText("");
                                fcontent_pdf.setText("");
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alertD = alertDialogBuilder.create();

        alertD.show();

    }


}
