package com.pdf_master;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class RecentsDocs extends Fragment {
    private GridView gridView;
    private Selected_Adapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recents_tab, container, false);
        gridView = view.findViewById(R.id.gridView);
        adapter = new Selected_Adapter(getActivity(), Documents.recent_files_array, Documents.recent_files_array_names, 1);
        gridView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}