package com.pdf_master;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class GallerySignature extends Activity {
    private List<SampleModel> sampleList;
    private ArrayList<String> Gallery_allimages;
    private DetailAdapter adapter;
    private GridView gridView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_signature);
        gridView = (GridView) findViewById(R.id.gallerygridview);
        Gallery_allimages = Utilities.getCameraImages(GallerySignature.this);
        sampleList = new ArrayList<>();
        for (int j = 0; j < Gallery_allimages.size(); j++) {
            sampleList.add(new SampleModel(Gallery_allimages.get(j), false));
        }
        adapter = new DetailAdapter(GallerySignature.this, R.layout.grid_items,
                sampleList);
        gridView.setAdapter(adapter);

    }


}
