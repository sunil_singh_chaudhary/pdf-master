package com.pdf_master;

public class SampleModel {

	String name;

	boolean isChecked;
	
	

	
	public SampleModel(boolean isChecked) {
		super();
		this.isChecked = isChecked;

	}
	public SampleModel() {
		super();
	

	}
	public SampleModel(String name, boolean isChecked) {
		super();
		this.name = name;
		this.isChecked = isChecked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	

}
