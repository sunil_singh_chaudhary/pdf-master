package com.pdf_master;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

public class Utilities {
    public static InterstitialAd mInterstitialAd1;
    public static AdRequest adRequest;


    public static ArrayList<String> getCameraImages(Context context) {
        String[] columns = new String[]{ImageColumns._ID, ImageColumns.TITLE,
                ImageColumns.DATA, ImageColumns.MIME_TYPE, ImageColumns.SIZE};

        final Cursor cursor = context.getContentResolver().query(
                Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, null);
        ArrayList<String> result = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor
                    .getColumnIndexOrThrow(Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);

                result.add(data);
                //Log.e("RESULT", data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return result;
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

   /* public static void InterstialAddMobb1(Context ctx) {
        Log.e("INSISDE ADMOB", "ADDMOB");
        adRequest = new AdRequest.Builder()
                .addTestDevice("1BB63F8D3CD3E93F0610A78AC0AC4D33")
                .build();
        mInterstitialAd1 = new InterstitialAd(ctx);
        mInterstitialAd1.setAdUnitId("ca-app-pub-9364142133685560/7518519739");
        mInterstitialAd1.loadAd(adRequest);
    }*/

}
