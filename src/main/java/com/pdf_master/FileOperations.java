package com.pdf_master;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

public class FileOperations {
    private int red ;
    private int green;
    private int blue ;
    private FontFamily family;
    public FileOperations() {
    }
    public Boolean write(String fname, String heading, String fcontent, int fontfamilytype, int fontsize, int fontstyle, int fontcolor) {
        try {
            Log.e("F==="+heading,"fontfamilytype"+fontfamilytype+"fontsize"+fontsize+"fontstyle"+fontstyle+"fontcolor"+fontcolor+"");
            String folderpath="/sdcard/Awwsom_pdf";
            String fpath = "/sdcard/Awwsom_pdf/" + fname + ".pdf";
            File folder=new File(folderpath);
            if (!folder.exists())
            {
                folder.mkdirs();
            }
            File file = new File(fpath);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
                Log.e("PATH CREATED IS=",fpath);
            // step 1
            Document document = new Document();
            // step 2
            PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));
            // step 3
            document.open();
            // step 4
     /*       if (fontfamilytype==0)
            {
                 family=FontFamily.TIMES_ROMAN;
            }
            if (fontfamilytype==1)
            {
                family=FontFamily.ZAPFDINGBATS;
            }
            if (fontfamilytype==2)
            {
                family=FontFamily.UNDEFINED;
            }
            if (fontfamilytype==3)
            {
                family=FontFamily.HELVETICA;
            }
            if (fontfamilytype==4)
            {
                family=FontFamily.COURIER;
            }
            if (fontfamilytype==5)
            {
                family=FontFamily.SYMBOL;
            }
            red = Color.red(fontcolor);
            green = Color.green(fontcolor);
            blue = Color.blue(fontcolor);
            Log.d("red", "red");
            Log.d("green", "green");
            Log.d("blue", "blue");*/
         //   Font f=new Font(FontFamily.TIMES_ROMAN,10.f,Font.BOLD, new BaseColor(red,green,blue));
            Font f=new Font(FontFamily.TIMES_ROMAN,30.f,Font.NORMAL,new BaseColor(0,0,0));
            Paragraph p=new Paragraph(fcontent,f);
            p.setAlignment(Paragraph.ALIGN_LEFT);
            // step 4
            Font first_f=new Font(FontFamily.TIMES_ROMAN,50.f,Font.BOLD, new BaseColor(0,0,0));
            Paragraph p_first=new Paragraph(heading,first_f);
            p_first.setAlignment(Paragraph.ALIGN_CENTER);

            document.add(new Paragraph(p_first));
            document.add(new Paragraph(p));
            // step 5
            document.close();
            Log.d("Suceess", "Sucess");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public String read(String fname) {
        BufferedReader br = null;
        String response = null;
        try {
            StringBuffer output = new StringBuffer();
            String fpath = "/sdcard/" + fname + ".pdf";
            PdfReader reader = new PdfReader(new FileInputStream(fpath));
            PdfReaderContentParser parser = new PdfReaderContentParser(reader);
            StringWriter strW = new StringWriter();
            TextExtractionStrategy strategy;
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                strategy = parser.processContent(i,
                        new SimpleTextExtractionStrategy());
                strW.write(strategy.getResultantText());
            }
            response = strW.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        final int width = !drawable.getBounds().isEmpty() ? drawable
                .getBounds().width() : drawable.getIntrinsicWidth();
        final int height = !drawable.getBounds().isEmpty() ? drawable
                .getBounds().height() : drawable.getIntrinsicHeight();
        final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width,
                height <= 0 ? 1 : height, Bitmap.Config.ARGB_8888);
        Log.v("Bitmap width - Height :", width + " : " + height);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    
    public static void stampIgnoreRotation(String src, String dest)
            throws IOException, DocumentException {
    	  PdfReader reader = new PdfReader(src); // input PDF
          PdfStamper stamper = new PdfStamper(reader,
            new FileOutputStream(dest)); // output PDF
          BaseFont bf = BaseFont.createFont(
                  BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED); // set font
          //loop on pages (1-based)
          for (int i=1; i<=reader.getNumberOfPages(); i++){
              // get object for writing over the existing content;
              // you can also use getUnderContent for writing in the bottom layer
              PdfContentByte over = stamper.getOverContent(i);
              // write text
              over.beginText();
              over.setFontAndSize(bf, 10);    // set font and size
            //  over.setTextMatrix(107, 740);   // set x,y position (0,0 is at the bottom left)
              over.showText("I can write at page " + i);  // set text
              over.endText();
              // draw a red circle
              over.setRGBColorStroke(0xFF, 0x00, 0x00);
              over.setLineWidth(5f);
              over.ellipse(250, 450, 350, 550);
              over.stroke();
          }

          stamper.close();
        }
    
    
    
    public static void absText(String path, int x, int y) {   //set text on click position
    	// step 1
        Document document = new Document();
        Font times = null ;
        // step 2
        PdfWriter writer;
		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(path));
		     // step 3
	        document.open();
	        // step 4
	        Chunk c;
	        String foobar = "Foobar Film Festival";
	        // Measuring a String in Helvetica
	        Font helvetica = new Font(FontFamily.HELVETICA, 12);
	        BaseFont bf_helv = helvetica.getCalculatedBaseFont(false);
	        float width_helv = bf_helv.getWidthPoint(foobar, 12);
	        c = new Chunk(foobar + ": " + width_helv, helvetica);
	        document.add(new Paragraph(c));
	        document.add(new Paragraph(String.format("Chunk width: %f", c.getWidthPoint())));
	        // Measuring a String in Times
	        BaseFont bf_times;
			try {
				//Typeface fot = Typeface.createFromAsset(co, path)
				bf_times = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
				  times = new Font(bf_times, 12);
			        float width_times = bf_times.getWidthPoint(foobar, 12);
			        c = new Chunk(foobar + ": " + width_times, times);
			        document.add(new Paragraph(c));
			        document.add(new Paragraph(String.format("Chunk width: %f", c.getWidthPoint())));
			        document.add(Chunk.NEWLINE);
			        // Ascent and descent of the String
			        document.add(new Paragraph("Ascent Helvetica: "
			                + bf_helv.getAscentPoint(foobar, 12)));
			     
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	        document.add(Chunk.NEWLINE);
	        // Kerned text
	        width_helv = bf_helv.getWidthPointKerned(foobar, 12);
	        c = new Chunk(foobar + ": " + width_helv, helvetica);
	        document.add(new Paragraph(c));
	        // Drawing lines to see where the text is added
	        PdfContentByte canvas = writer.getDirectContent();
	        canvas.saveState();
	        canvas.setLineWidth(0.05f);
	        canvas.moveTo(400, 806);
	        canvas.lineTo(400, 626);
	        canvas.stroke();
	        canvas.restoreState();
	        // Adding text with PdfContentByte.showTextAligned()
	        canvas.beginText();
	        canvas.setFontAndSize(bf_helv, 12);
	        canvas.showTextAligned(Element.ALIGN_LEFT, foobar, 400, 788, 0);
	        canvas.endText();
	        // More lines to see where the text is added
	        canvas.saveState();
	        canvas.setLineWidth(0.05f);
	        canvas.moveTo(200, 590);
	        canvas.lineTo(200, 410);
	        canvas.stroke();
	        canvas.restoreState();
	        // Adding text with ColumnText.showTextAligned()
	        Phrase phrase = new Phrase(foobar, times);
	        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, 200, 572, 0);
	      
	        // Chunk attributes
	        c = new Chunk(foobar, times);
	        c.setHorizontalScaling(0.5f);
	        phrase = new Phrase(c);
	        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, 400, 428, -0);
	        // step 5
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   
        document.close();
      }
}