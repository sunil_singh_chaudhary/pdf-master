package com.pdf_master;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DetailAdapter extends ArrayAdapter<SampleModel> {
    private Context ctx = null;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private List<SampleModel> items;
    private View v;

    public DetailAdapter(Context c, int textViewResourceId,
                         List<SampleModel> items) {
        super(c, textViewResourceId, items);
        ctx = c;

        this.items = items;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(ctx));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.ic_menu_gallery)
                .showImageForEmptyUri(android.R.drawable.ic_menu_gallery)
                .showImageOnFail(android.R.drawable.ic_menu_gallery).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).build();


    }

    @Override
    public SampleModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {

        return super.getItemId(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RecordHolder holder;
        v = convertView;
        if (v == null) {
            LayoutInflater mInflater = ((Activity) ctx).getLayoutInflater();
            v = mInflater.inflate(R.layout.grid_items, parent, false);
            holder = new RecordHolder();
            holder.imagev = (ImageView) v.findViewById(R.id.imag_v1);
            holder.select_imageview = (ImageView) v
                    .findViewById(R.id.select_imageview);
            holder.select_imageview.setTag(position);
            v.setTag(holder);

        } else {
            holder = (RecordHolder) v.getTag();

        }
        holder.imagev.setId(position);
        holder.select_imageview.setId(position);
        imageLoader.displayImage("file://" + items.get(position).getName(),
                holder.imagev, options, animateFirstListener);


        holder.imagev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Patho of pdf===", items.get(position).getName());
                addSignature(items.get(position).getName());

                Thread x = new Thread() {
                    public void run() {
                        try {
                            Log.d("", "local Thread sleeping");
                            Thread.sleep(2000);
                            Intent i = new Intent(ctx, MainActivity.class);
                            ctx.startActivity(i);
                        } catch (InterruptedException e) {
                            Log.e("", "local Thread error", e);
                        }
                    }};
                x.run();
            }
        });
        return v;

    }


    static class RecordHolder {

        ImageView imagev;
        ImageView select_imageview;
    }

    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {
        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 400);
                    displayedImages.add(imageUri);
                }
            }
        }
    }


    private void addSignature(String imagepath) {
        try {
            SharedPreferences preferences = ctx.getSharedPreferences(Selected_Adapter.HEADER_PATH, ctx.MODE_PRIVATE);
            // then you use
            String path = preferences.getString(Selected_Adapter.HEADER_PATH_GET, "");
            String SaveStampedPathName = "/pdf/Signed" + System.currentTimeMillis() + ".pdf";
            PdfReader pdfReader = new PdfReader(path);
            PdfStamper pdfStamper = new PdfStamper(pdfReader,
                    new FileOutputStream(Environment.getExternalStorageDirectory() + SaveStampedPathName));
            Log.e("Image Path==", imagepath);
            InputStream ims = new FileInputStream(imagepath);
            //Context.getAssets().open("watermark.jpg");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            Bitmap resized = Bitmap.createScaledBitmap(bmp, 150, 150, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                PdfContentByte content = pdfStamper.getUnderContent(i);
                image.setAbsolutePosition(20f, 20f);
                content.addImage(image);
            }

            pdfStamper.close();
            Toast.makeText(ctx, ctx.getString(R.string.savedsign) + SaveStampedPathName, Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

}
