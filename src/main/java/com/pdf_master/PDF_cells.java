package com.pdf_master;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;

import java.io.IOException;

/**
 * Created by therock on 7/12/2016.
 */
public  class PDF_cells {

    public static PdfPCell createImageCell(Image path) throws DocumentException, IOException {
        Image img = path;
        PdfPCell cell = new PdfPCell(img, true);
        return cell;
    }

    public static PdfPCell createTextCell(String text) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell(new Phrase("HEADER", FontFactory.getFont(FontFactory.HELVETICA, 18)));
        Paragraph p = new Paragraph(text);

        p.setAlignment(Element.ALIGN_BOTTOM);
        cell.addElement(p);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }
}
